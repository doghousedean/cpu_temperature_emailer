#!/usr/bin/env python2

## Python script to check CPU Temp on Ubuntu 18.04 and email a notification if greater than a specified value

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import smtplib
import sys
import time
import os 
import requests
import yaml
import psutil
import json

dir_path = os.path.dirname(os.path.realpath(__file__))

## Get config

with open(dir_path + '/config.yaml', 'r') as config_file:
    cfg = yaml.load(config_file)

config_file.close()

## Variables for cusomisation
max_cpu_temp = cfg['options']['max_cpu']
max_timeout = cfg['options']['max_timeout']
cpu_temp_device = cfg['options']['cpu_temp_device']
fromaddr = cfg['email']['from_addr']
toaddr = cfg['email']['to_addr']
email_host = cfg['email']['host']
email_host_port = cfg['email']['port']
speedtest_freq = cfg['options']['speedtest_freq']
speedtest_d_json = cfg['options']['speedtest_d_json']
speedtest_u_json = cfg['options']['speedtest_u_json']

## Change to 1 to send data to an InfluxDB (for grafana)
use_cpu_temp = cfg['options']['use_cpu_temp']
use_cpu_usage = cfg['options']['use_cpu_usage']
use_speedtest = cfg['options']['use_speedtest']
cpu_temp = 1
# InfluxDB details
IP = cfg['influxdb']['host']                # The IP of the machine hosting your influxdb instance
DB = cfg['influxdb']['db']                  # The database to write to, has to exist
USER = cfg['influxdb']['username']          # The influxdb user to authenticate with
PASSWORD = cfg['influxdb']['password']      # The password of that user
SOURCE_HOST = cfg['influxdb']['src_host']   # The hostname of the source of the data

def post_to_influx(field,value):
    ## ts in seconds converted to nano seconds for influxdb
    dataset = str(field) + ",host=" + SOURCE_HOST + " value=" + str(value)
    try:
        r2 = requests.post("http://%s:8086/write?db=%s" %(IP, DB), auth=(USER, PASSWORD), data=dataset)
    except ConnectionError as e:
        pass

def send_email(temp):
    # Sends an email
    global fromaddr,toaddr,email_host,email_host_port
    # Set email parameters
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = 'CPU Temperature ALERT'
    
    # Load email body template
    
    email_body_fh = open(dir_path + '/email_template.html','r')
    email_body = email_body_fh.read()
    email_body_fh.close()
    body = email_body.replace("<temp>",str(temp))
    body = body.replace("<max_cpu>",str(max_cpu))
    # Crafting email
    
    msg.attach(MIMEText(body,'html'))
    server = smtplib.SMTP(email_host,email_host_port)
    server.ehlo()
    server.starttls()
    server.ehlo()
    text = msg.as_string()
    server.sendmail(fromaddr,toaddr,text)

def save_ts():
    # Save last timestamp to file
    ts = open(dir_path + "/sensor_ts.dat",'w+')
    now = time.time()
    ts.write("%s\n" % now)
    ts.close()

def save_to_log(temp):
    # Add to log file
    global max_cpu
    lf = open(dir_path + "/sensor.log",'a+')
    now = time.time()
    lf.write("%s : Sensor triggered, max: %s value: %s\n" % (now,max_cpu,temp))
    lf.close()

def save_temp_to_file(temp):
    ## Save data to CSV in case we want to graph it later
    tf = open(dir_path + "/sensor.dat",'a')
    now = time.time()
    tf.write("%s,%s\n" % (now,temp))
    tf.close()

def get_cpu_temp():
    # Get CPU temperature
    fh = open(cpu_temp_device,'r')
    
    cpu_temp = float(fh.read().rstrip()) / 1000
    
    fh.close()
    
    post_to_influx('cpu',cpu_temp)
    save_temp_to_file(cpu_temp)

def get_cpu_usage():
    cores = psutil.cpu_count()
    c_data = psutil.cpu_percent(interval=1, percpu=True)
    c_total = 0
    for c in range(cores):
        post_to_influx('cpu_core_{}'.format(c),c_data[c-1])
        c_total = c_total + c_data[c-1]
    c_avg = c_total / cores
    post_to_influx('cpu_avg',c_avg)

def get_speedtest():
    stdfile = open(speedtest_d_json,'r')
    stufile = open(speedtest_u_json,'r')
    stddata = json.load(stdfile)
    studata = json.load(stufile)
    sum_sent = stddata['end']['sum_received']['bits_per_second']
    sum_received = studata['end']['sum_received']['bits_per_second']
    post_to_influx('speedtest_sent',sum_sent)
    post_to_influx('speedtest_received',sum_received)

if use_speedtest:
    get_speedtest()

## Check if CPU Usage is enabled
if use_cpu_usage:
    get_cpu_usage()

## Check if CPU Temp is enabled
if use_cpu_temp:
    get_cpu_temp()

if cpu_temp > max_cpu_temp - 1:
    now = time.time()
    try:
        time_fh = open(dir_path + "/sensor_ts.dat",'r')
        last_email = float(time_fh.read().rstrip())
        time_fh.close()
    except IOError:
        last_email = 0
    
    if now > last_email + max_timeout:
        send_email(cpu_temp)
        save_ts()
        save_to_log(cpu_temp)
