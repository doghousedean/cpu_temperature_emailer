# CPU Temperature Email notifier

**TODO Update README for new config options**

This is a quick and dirty script to email me if my server gets too hot and also send the data to an InfluxDB so I can graph it because why not?

No need for nagios install when I only need stat to check

## Speedtest checks

These require iperf3 binaries for now

Set up the files in the config.yaml, could be in /tmp too

Edit the runspeedtests.sh file to input your iperf server IP

Then create a crontab entry to run the speed tests every 10 mins or so

~~~~
*/10 * * * * python /home/user/cpu_sensor_emailer/runspeedtests.sh
~~~~

## Usage

First find your CPU temperature device.


~~~~
cd /sys/class/thermal/
ls -l thermal*
lrwxrwxrwx 1 root root 0 Oct 16 13:22 thermal_zone0 -> ../../devices/virtual/thermal/thermal_zone0
lrwxrwxrwx 1 root root 0 Oct 16 13:22 thermal_zone1 -> ../../devices/virtual/thermal/thermal_zone1
~~~~

From this we can see two thermal_zones, now we check the temperature of each one to determine which is the correct sensor.

The output is in millidegrees.


~~~~
cat thermal_zone0/temp
8300
~~~~

This shows 8.3 Degrees C, this doesn't look right, lets try the next one.


~~~~
cat thermal_zone1/temp
35000
~~~~

That's better, 35 Degrees C.  We now have the location of our sensor, we can update the script's **cpu_temp_device** variable to the data below


~~~~
/sys/class/thermal/thermal_zone1/temp
~~~~

The next job is to update the details for your mail service either a local one or from your ISP, gmail the choice is your.

## Crontab to run the check

~~~~
crontab -e
~~~~

This will edit the crontab file for the current user, there are lots of guides on cron files so I will just give my own as an example of running the check every 2 minutes

~~~~
*/2 * * * * python /home/user/cpu_sensor_emailer/sensors
~~~~

## Options

There are a couple of editable options in the script, mainly the temperature at which the email is triggered and how long after the last notification is another sent

See source for details